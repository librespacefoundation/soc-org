### Sample Idea name.

**Aim**  Please describe the scope of the idea at hand.

**Skills/Knowledge required**
 * p.e. Python
 * p.e. GNU Radio flowgraphs

**Estimated time effort**
 * 90 hours
 * or 175 hours 
 * or 350 hours 
 * or any mix of the above

**Estimated length**
 * in weeks (standard is 12)

**Expected results** What should the finalized contribution is expected to do

**Potential mentor(s)** you know who you are (if you are unsure about mentors leave this empty and maybe a mentor will come forward in the relevant issue)

**Related repositories** add if applicable

**Relate chatroom** add if applicable 
